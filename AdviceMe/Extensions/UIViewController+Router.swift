//
//  UIViewController+Router.swift
//  AdviceMe
//
//  Created by vitalii on 27.04.2022.
//

import UIKit
import Route

// MARK: - UIViewController

extension UIViewController {
    
    // MARK: - Public properties

    var router: Router { Router(window: UIApplication.shared.windows[0], controller: self) }
}

// MARK: - Router

extension Router {
    
    // MARK: - Public methods
    
    
    
    func openInitialController() {
        let initialController = InitialController.initiate()
        openController(initialController, withNavigation: false)
    }
    
    
    
    
    
    

    // MARK: - Private methods
    
    private func openController(_ controller: UIViewController, withNavigation: Bool = false) {
        let controller = withNavigation ? UINavigationController(rootViewController: controller) : controller
        
        setWindowRoot(controller, animated: false)
        UIApplication.shared.delegate?.window??.makeKeyAndVisible()
    }
}
