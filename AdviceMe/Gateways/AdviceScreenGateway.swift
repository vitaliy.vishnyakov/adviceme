//
//  AdviceScreenGateway.swift
//  AdviceMe
//
//  Created by vitalii on 27.04.2022.
//

import Foundation

final class AdviceScreenGateway {
    
    let url = URL(string: Environments.apiURL)
    
    func getAdviceWithId(completion: @escaping (AdviceEntity) -> Void){
        guard let safeUrl = url else {
            return
        }
        
        URLSession(configuration: .default).dataTask(with: safeUrl){
            data, response, error in
            if error == nil{
                guard let safeData = data else {
                    return
                }
                guard let info = try? JSONDecoder().decode(AdviceEntity.self, from: safeData) else {
                    return
                }
                DispatchQueue.main.async {
                    completion(info)
                }
            }else{
                DispatchQueue.main.async {
                    completion(AdviceEntity(slip: Slip(id: -1, advice: "Error(")))
                }
            }
        }.resume()
    }
}
