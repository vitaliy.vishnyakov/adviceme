//
//  CustomError.swift
//  AdviceMe
//
//  Created by vitalii on 27.04.2022.
//

import Foundation

enum CustomError: Error {
    case fetchError
}
