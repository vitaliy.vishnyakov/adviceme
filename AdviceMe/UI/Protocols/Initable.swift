//
//  Initable.swift
//  Sever-minerals
//
//  Created by Denis Sushkov on 03.11.2021.
//

import UIKit

// MARK: - Initable

protocol Initable {

    static func initiate() -> Self
}

// MARK: - XibInitable

protocol XibInitable: Initable {

    static var xibName: String { get }
}

// MARK: - Default Implementation

extension XibInitable {
    
    // MARK: - Public methods

    static var xibName: String {
        return String(describing: Self.self)
    }
}

// MARK: - UIViewController Default Implementation

extension XibInitable where Self: UIViewController {
    
    // MARK: - Public methods

    static func initiate() -> Self {
        return Self(nibName: xibName, bundle: nil)
    }
}

// MARK: - UIView Default Implementation

extension XibInitable where Self: UIView {
    
    // MARK: - Public methods

    // swiftlint:disable force_cast
    static func initiate() -> Self {
        let instance = UINib(nibName: xibName, bundle: nil)
            .instantiate(withOwner: nil, options: nil)
            .first as! Self

        return instance
    }
}

// MARK: - CodeInitable

protocol CodeInitable: Initable { }

// MARK: - UIViewController Default Implementation

extension CodeInitable where Self: UIViewController {
    
    // MARK: - Public methods

    static func initiate() -> Self {
        return Self()
    }
}

// MARK: - UIView Default Implementation

extension CodeInitable where Self: UIView {
    
    // MARK: - Public methods

    static func initiate() -> Self {
        return Self()
    }
}
