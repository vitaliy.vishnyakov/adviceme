//
//  InitialController.swift
//  AdviceMe
//
//  Created by vitalii on 27.04.2022.
//

import UIKit

class InitialController: UIViewController, CodeInitable {

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .red
        
        openAdivceController()
    }
    
    private func openAdivceController (){
        let adviceController = AdviceController.initiate()
        adviceController.modalPresentationStyle = .fullScreen
        DispatchQueue.main.asyncAfter(deadline: .now() ) { [weak self] in
            self?.router.present(adviceController)
        }
    }
    
}
