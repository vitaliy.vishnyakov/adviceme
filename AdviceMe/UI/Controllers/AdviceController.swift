//
//  AdviceController.swift
//  AdviceMe
//
//  Created by vitalii on 27.04.2022.
//

import UIKit

class AdviceController: UIViewController, CodeInitable {
    
    private enum Constants {
        static let labelOffsetY = 100
        static let labelNumberOfLines = 0
        static let labelWidth = 200
        static let labelHeight = 100
        static let buttonWidth = 200
        static let buttoHeight = 100
        static let buttonCornerRadius = 16
    }
    
    private let adviceScreenInteractor: AdviceScreenInteractor = AdviceScreenInteractor()
    private let button: UIButton = UIButton()
    private let label: UILabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        view.addSubview(button)
        view.addSubview(label)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        configurateLabel()
        
        configurateButton()
    }
    
    @objc private func buttonAction(sender: UIButton!) {
        adviceScreenInteractor.getAdvice{ [weak self] info in
            self?.updateAdviceText(with: info)
        }
    }
    
    private func updateAdviceText(with text: String){
        self.label.text = text
    }
    
    private func configurateLabel(){
        label.numberOfLines = Constants.labelNumberOfLines
        label.center.y = view.center.y + CGFloat( Constants.labelOffsetY)
        label.center.x = view.center.x
        label.bounds.size = CGSize(width: Constants.labelWidth, height: Constants.labelHeight)
        label.textAlignment = .center
    }
    
    private func configurateButton(){
        button.center = view.center
        button.layer.cornerRadius = CGFloat(Constants.buttonCornerRadius)
        button.backgroundColor = .black
        button.bounds.size = CGSize(width: Constants.buttonWidth, height: Constants.buttoHeight)
        button.setTitle(R.string.ui.getAdvice(), for: .normal)
        button.setTitle(R.string.ui.waitAdvice(), for: .highlighted)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
    }
    
    
    
    


}
