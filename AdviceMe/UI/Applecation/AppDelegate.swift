//
//  AppDelegate.swift
//  AdviceMe
//
//  Created by vitalii on 27.04.2022.
//

import UIKit
import Route
@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    lazy var window: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
    lazy var router = Router(window: window)
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        router.openInitialController()
        
        return true
    }

}

