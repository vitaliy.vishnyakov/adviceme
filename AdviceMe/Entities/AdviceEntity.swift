//
//  AdviceEntity.swift
//  AdviceMe
//
//  Created by vitalii on 27.04.2022.
//

import Foundation


struct AdviceEntity: Codable {
    let slip: Slip
}

struct Slip: Codable {
    let id: Int
    let advice: String
}


