//
//  AdviceScreenInteractor.swift
//  AdviceMe
//
//  Created by vitalii on 27.04.2022.
//

import Foundation

final class AdviceScreenInteractor {
    
    private var adviceScreenGateway = AdviceScreenGateway()
    
    func getAdvice(completion: @escaping (String) -> Void){
        adviceScreenGateway.getAdviceWithId{ result in
            completion(result.slip.advice)
        }
    }
}
