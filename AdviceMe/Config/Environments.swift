//
//  Environments.swift
//  Sever-minerals
//
//  Created by Denis Sushkov on 01.11.2021.
//

import Foundation

// swiftlint:disable all

// MARK: - Instance

enum Instance: String {
    case dev
    case prod
    case release
    
    var isRelease: Bool { self == .release }
}

// MARK: - LosslessStringConvertible

extension Instance: LosslessStringConvertible {
    
    var description: String { rawValue }
    
    init?(_ description: String) {
        self.init(rawValue: description)
    }
}

// MARK: - Environments

final class Environments {
    
    // MARK: - ConfigKey
    
    enum ConfigKey: String, CaseIterable {
        case apiURL = "API_URL"
        
    }
    
    // MARK: - Public properties

    static var apiURL: String { value(for: .apiURL)! }
    
    // MARK: - Public methods
    
    static func value<T: LosslessStringConvertible>(for key: Environments.ConfigKey) -> T? {
        return try? Configuration.value(for: key.rawValue)
    }
    
    // MARK: - Private methods
    
    private static func checkConfiguration() {
        print("\n---Begin setup environments---")
        print("XCConfig:")
        
        ConfigKey.allCases.forEach { (key) in
            Configuration.checkValue(for: key.rawValue)
        }
        print("---End setup environments---\n")
    }
}
